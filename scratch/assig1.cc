#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/internet-module.h"
#include "ns3/radvd-interface.h"
#include "ns3/radvd-prefix.h"
#include "ns3/radvd.h"

#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("Assignment1");

int main(int argc, char** argv)
{
    Ptr<Node> n0 = CreateObject<Node>();
    Ptr<Node> n1 = CreateObject<Node>();
    Ptr<Node> r = CreateObject<Node>();
    Ptr<Node> n2 = CreateObject<Node>();

    NodeContainer net1(n0, r, n2);
    NodeContainer net2(n1, r);
    NodeContainer all(n0, r, n1, n2);

    // Installa Internet IPv6
    InternetStackHelper i6;
    i6.Install(all);

    // Canale CSMA
    CsmaHelper csma;
    csma.SetChannelAttribute("DataRate", DataRateValue(100000));
    csma.SetChannelAttribute("Delay", TimeValue(MilliSeconds(5)));
    NetDeviceContainer d1 = csma.Install(net1);
    NetDeviceContainer d2 = csma.Install(net2);

    // Assegna gli indirizzi IPv6
    Ipv6AddressHelper ipv6;

    // Subnet n0
    ipv6.SetBase(Ipv6Address("2001:1::"), Ipv6Prefix(64));
    NetDeviceContainer dev0;
    dev0.Add(d1.Get(0));
    dev0.Add(d1.Get(2));
    Ipv6InterfaceContainer iic1 = ipv6.Assign(dev0);

    NetDeviceContainer rou1;
    rou1.Add(d1.Get(1));
    Ipv6InterfaceContainer iicr1 = ipv6.Assign(rou1);
    iicr1.SetForwarding(0, true);
    iic1.Add(iicr1);

    // Subnet n1
    ipv6.SetBase(Ipv6Address("2001:2::"), Ipv6Prefix(64));
    NetDeviceContainer dev1;
    dev1.Add(d2.Get(0));
    Ipv6InterfaceContainer iic2 = ipv6.Assign(dev1);

    NetDeviceContainer rou2;
    rou2.Add(d2.Get(1));
    Ipv6InterfaceContainer iicr2 = ipv6.Assign(rou2);
    iicr2.SetForwarding(0, true);
    iic2.Add(iicr2);

    // Router Advertisement
    RadvdHelper radvdHelper;

    radvdHelper.AddAnnouncedPrefix(iic1.GetInterfaceIndex(1), Ipv6Address("2001:1::0"), 64);
    radvdHelper.AddAnnouncedPrefix(iic2.GetInterfaceIndex(1), Ipv6Address("2001:2::0"), 64);

    ApplicationContainer radvdApps = radvdHelper.Install(r);
    radvdApps.Start(Seconds(1.0));
    radvdApps.Stop(Seconds(10.0));

    // PING
    uint32_t packetSize = 1024;
    uint32_t maxPacketCount = 5;

    Ipv6Address ind0 = iic1.GetAddress(0, 1);
    std::cout << "Indirizzo n0: " << ind0 << std::endl;

    Ipv6Address ind1 = iic2.GetAddress(0, 1);
    std::cout << "Indirizzo n1: " << ind1 << std::endl;

    Ipv6Address ind2 = iic1.GetAddress(1, 1);
    std::cout << "Indirizzo n2: " << ind2 << std::endl;

    Ipv6Address indr = iic1.GetAddress(2, 1);
    std::cout << "Indirizzo router: " << indr << std::endl;

    PingHelper ping1(ind1);  // n0 ping n1
    PingHelper ping2(ind2);  // n0 ping n2

    ping1.SetAttribute("Count", UintegerValue(maxPacketCount));
    ping1.SetAttribute("Size", UintegerValue(packetSize));
    ApplicationContainer p0 = ping1.Install(net1.Get(0));

    ping2.SetAttribute("Count", UintegerValue(maxPacketCount));
    ping2.SetAttribute("Size", UintegerValue(packetSize));
    ApplicationContainer p2 = ping2.Install(net1.Get(0));

    p0.Start(Seconds(2.0));
    p2.Start(Seconds(2.0));

    p0.Stop(Seconds(7.0));
    p2.Stop(Seconds(7.0));

    csma.EnablePcapAll(std::string("pcap"), false);

    NS_LOG_INFO("Esecuzione della Simulazione.");
    Simulator::Run();
    Simulator::Destroy();
    NS_LOG_INFO("Completato.");

    return 0;
}
