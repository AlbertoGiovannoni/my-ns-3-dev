/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"

// Default Network Topology
//
//       10.1.1.0
// n0 -------------- n1
//    point-to-point
//

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FirstScriptExample"); // crea una classe di tipo LOG_COMPONENT con il nome del file e lo salva in una variabile sattica g_log (vedi f12)
// in una classe posso avere solo un NS_LOG_COMPONENT_DEFINE
int
main(int argc, char* argv[])
{
    CommandLine cmd(__FILE__);
    cmd.Parse(argc, argv);

    Time::SetResolution(Time::NS);  // definisce l'unità minima di tempo rappresentabile
    LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO); // LOG_LEVEL definisce il livello di dettaglio con cui vengono restituite le stampe su terminale
    LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);

    NodeContainer nodes; // è il container di una lista
    nodes.Create(2); // riempe il container con 2 istanze della classe nodes (classe da considerare come una scatola in cui si possono mettere altre classi su cui simulare problemi di rete)
    PointToPointHelper pointToPoint; // le classi che finiscono con helper suggeriscono che devono essere usate. Serve  creare nell'ordine giusto le componenti che servono per far funzionare il modello
    pointToPoint.SetDeviceAttribute("DataRate", StringValue("5Mbps")); // imposta il data rate
    pointToPoint.SetChannelAttribute("Delay", StringValue("2ms")); // imposta il delay

    NetDeviceContainer devices; // un netDevice è un dispositivo di rete dal livello 2 in giù, simula il livello MAC e il Fisico.
    devices = pointToPoint.Install(nodes); // crea un link point to point fra i due nodi e restituisce un container dei due nodi linkati
    // l'interfaccia point to point non ha l'arp perchè è una seriale
    InternetStackHelper stack; // installa lo stack TCP/IP
    stack.Install(nodes); // installa lo stack su i due nodi. Installa anche di default IPV4 e IPV6 ma con indirizzi di default

    Ipv4AddressHelper address; // da ai nodi degli indirizzi IPV4. è come assegnare gli indirizzi alle macchine manualmente. Se si fanno dispositivi mobili, l'IP rischia di non aver nulla a che fare con la rete a cui è connesso
    address.SetBase("10.1.1.0", "255.255.255.0"); // assegne gli indirizzi ai netDevice

    Ipv4InterfaceContainer interfaces = address.Assign(devices); 
    // mini protocollo di livello 7 (applicazione) che genera del traffico
    UdpEchoServerHelper echoServer(9); // ascolta su una porta e quando riceve un pacchetto lo inoltra

    ApplicationContainer serverApps = echoServer.Install(nodes.Get(1)); // installo il server su un nodo 
    serverApps.Start(Seconds(1.0)); 
    serverApps.Stop(Seconds(10.0)); // ferma tutto anche se ho eventi in coda. ferma il server

    UdpEchoClientHelper echoClient(interfaces.GetAddress(1), 9);
    echoClient.SetAttribute("MaxPackets", UintegerValue(1));
    echoClient.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    echoClient.SetAttribute("PacketSize", UintegerValue(1024));

    ApplicationContainer clientApps = echoClient.Install(nodes.Get(0)); // installo i client su un nodo
    clientApps.Start(Seconds(2.0)); //gli eventi vengono schedulati, visto che la simulazione è discrete event. Start innesca il primo evento 
    clientApps.Stop(Seconds(10.0)); // ferma tutto anche se ho eventi in coda. ferma il client

    Simulator::Stop(); // buona pratica, assicura che oltre il tempo stabilito si fermi tutto, perche potrei avere protocolli attivi di IPV6 ecc che non fanno finire la simulazione
    Simulator::Run(); // avvia la simulazione e termina quando non ci sono più eventi
    Simulator::Destroy(); // distrugge tutto ciò che è stato creato durante la simulazione
    return 0;
}
